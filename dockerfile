FROM node:16-alpine3.11

#ENV ENDPOINT= 'xxxxxx'
#ENV ROX_API_TOKEN=xxxxxx
ENV SNYK_TOKEN=cd1f59be-150d-42a3-b84d-b0e478034e03

RUN apk update
RUN npm install -g snyk
RUN snyk config set api=$SNYK_TOKEN
CMD ["/bin/sh"]